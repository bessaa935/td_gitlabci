TD GIT
######
s
.. contents::
  :backlinks: top


101:
####

Pour ceux qui ont besoin de revoir un peu les bases :
-----------------------------------------------------

go to see this `documentation`_.

.. _documentation: http://my-snippets-host-static-website.s3-website-eu-west-1.amazonaws.com/git/index.html#git

GIT HELP:
---------

A tout moment, vous pouvez obtenir de l’aide sur une commande git.

* git help commit
* git help push
* git help <command>

.. code-block:: bash

    git help
    usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
               [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
               [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
               [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
               <command> [<args>]

    These are common Git commands used in various situations:

    start a working area (see also: git help tutorial)
       clone      Clone a repository into a new directory
       init       Create an empty Git repository or reinitialize an existing one
    [...]


TD PART:
##########

1. Fork un projet en ligne de commande :

* Cloner un dépôt distant pour l’avoir en local

.. code-block:: bash

    git clone https://gitlab.com/esme5/td_git.git

* Voir le dépôt distant qui a été cloné

.. code-block:: bash

    git remote -v

* Supprimer le fichier .git à la racine de votre projet:

.. code-block:: bash

    rm -rf .git/

Réinitialiser un git répertoire avec :

.. code-block:: bash

    git init

Créer un repo distant avec un projet existant en local :

.. code-block:: bash

    git remote add origin git@gitlab.com:<YOUR_PSEUDO>/td_git.git


Ces lignes de commandes vous ont permis de reproduire un FORK !

2. Make a bugFix in isolated branch and merge it.

* Create a new branch called bugFix

* Commit once

* Go back to master with git checkout

* Commit another time

* Merge the branch bugFix into master with git merge

Result tree :

.. image:: img/td_part_2.png
  :width: 100


Once merged you can delete the remote & local branches with :

.. code-block:: bash

    git branch -a # we print out all the branches (local as well as remote)
    git push origin --delete <YOUR_BRANCH_NAME> # To delete remote branch
    git branch -d <YOUR_BRANCH_NAME> # To delete local branch


